package LOL;

public class Settings{

    public static int AddPolygonMutationRate = 70;
    public static int MovePolygonMutationRate = 70;
    public static int RemovePolygonMutationRate = 70;
    public static int ReplacePolygonMutationRate = 140;
    public static int SwapPolygonMutationRate = 70;

    public static int PointsPerPolygonMax = 15;
    public static int PointsPerPolygonMin = 3;
    
    public static int PolygonsMax = 250;
    public static int PolygonsMin = 10;
    
    public static int AddPointMutationRate = 140;
    public static int RemovePointMutationRate = 140;
    public static int SwapPointMutationRate = 140;

    public static int MovePointMaxMutationRate = 140;
    public static int MovePointMidMutationRate = 100;
    public static int MovePointMinMutationRate = 70;


    public static int MovePointRangeMid = 20;
    public static int MovePointRangeMin = 5;
    
    public static int AlphaMutationRate = 440;
    public static int RedMutationRate = 440;
    public static int BlueMutationRate = 440;
    public static int GreenMutationRate = 440;

    public static int AlphaShiftMutationRate = 440;
    public static int RedShiftMutationRate = 440;
    public static int BlueShiftMutationRate = 440;
    public static int GreenShiftMutationRate = 440;


    

}
