package LOL;

import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;


public class Renderer {
    private Image img;
    private Canvas canvas;
    private GraphicsContext g;
    private WritableImage display_image, hidden_image;
    private Drawing currentDraw;
    public double ErrorGap;

    public Renderer(Image img, Canvas canvas, GraphicsContext g, WritableImage display_image){
        this.img = img;
        this.canvas = canvas;
        this.g = g;
        this.display_image = display_image;
        hidden_image = new WritableImage(Tools.MaxW, Tools.MaxH);
    }


    public void initialize(int[][] sourcecolor){
        currentDraw = new Drawing();
        currentDraw.init();
        g.clearRect(0,0, Tools.MaxW, Tools.MaxH);
        for (Polygon polygon: currentDraw.Polygons) {
            polygon.draw(g);
        }
        canvas.snapshot(new SnapshotParameters(), hidden_image);
        ErrorGap = Calculator.getdrawing(hidden_image, sourcecolor);
    }

    //render a Drawing
    public void Render(int[][] sourcecolor){
        Drawing newDraw = currentDraw.clone();
        do {
            newDraw.Mutate();
        } while (!newDraw.IsDirty);

        g.clearRect(0,0, Tools.MaxW, Tools.MaxH);
        for (Polygon polygon: newDraw.Polygons) {
            polygon.draw(g);
        }
        canvas.snapshot(new SnapshotParameters(), hidden_image);

        double newErrorGap = Calculator.getdrawing(hidden_image, sourcecolor);

        if (newErrorGap < ErrorGap){
            currentDraw = newDraw;
            ErrorGap = newErrorGap;
            System.out.println(newDraw.Polygons_count);
        }
        //else, discard new drawing
    }

    //
    public void render_best_generation(){
        g.clearRect(0,0, Tools.MaxW, Tools.MaxH);
        for (Polygon polygon: currentDraw.Polygons) {
            polygon.draw(g);
        }
        canvas.snapshot(new SnapshotParameters(), display_image);
    }

    public void reset(int[][] sourcecolor){
        currentDraw.init();
        g.clearRect(0,0, Tools.MaxW, Tools.MaxH);
        for (Polygon polygon: currentDraw.Polygons) {
            polygon.draw(g);
        }
        canvas.snapshot(new SnapshotParameters(), hidden_image);
        ErrorGap = Calculator.getdrawing(hidden_image, sourcecolor);
        g.clearRect(0,0, Tools.MaxW, Tools.MaxH);
        canvas.snapshot(new SnapshotParameters(), display_image);
    }

}
