package LOL;

import java.awt.image.TileObserver;
import java.io.Serializable;
import java.util.ArrayList;

import static LOL.Settings.*;

public class Drawing implements Serializable {
    public ArrayList<Polygon> Polygons;
    public int Polygons_count = 0;

    public boolean IsDirty;

    public void setDirty(){
        IsDirty = true;
    }

    public void init(){
        Polygons = new ArrayList<Polygon>();
        Polygons_count = 0;
        for (int i = 0; i < PolygonsMin; i++){
             AddPol();
        }
        IsDirty = false;
    }

    public Drawing clone(){
        Drawing drawing = new Drawing();
        drawing.Polygons = new ArrayList<Polygon>();
        drawing.Polygons_count = Polygons_count;
        for (Polygon polygon: Polygons){
            drawing.Polygons.add(polygon.Clone());
        }
        drawing.IsDirty = false;
        return drawing;
    }

    public void Mutate(){
        this.IsDirty = false;
        if (Tools.WillMutate(AddPolygonMutationRate))
            AddPol();

        if (Tools.WillMutate(RemovePolygonMutationRate))
            RevPol();

        if (Tools.WillMutate(MovePolygonMutationRate))
            MovePol();

        if (Tools.WillMutate(ReplacePolygonMutationRate)){
            ReplacePol();
        }

        if (Tools.WillMutate(SwapPolygonMutationRate)){
            SwapPol();
        }

        for (Polygon polygon:Polygons
             ) {
            polygon.Mutate(this);
        }
    }

    private void MovePol(){ //Move Polygon
        if (Polygons_count < 1){
            return;
        }
        int idx = Tools.GetRand(0, Polygons_count-1);
        Polygon polygon = Polygons.get(idx);
        Polygons.remove(idx);
        idx = (idx + Tools.GetRand(1, Polygons_count - 1)) % Polygons_count;
        if (idx == Polygons_count - 1) Polygons.add(polygon);
        else Polygons.add(idx, polygon);
        setDirty();
    }

    private void RevPol(){ //Remove Polygon
        if (Polygons_count > PolygonsMin){
            int idx = Tools.GetRand(0, Polygons.size()-1);
            Polygons.remove(idx);
            Polygons_count--;
            setDirty();
        }
    }

    private void AddPol(){ //Add Polygon
        if (Polygons_count < PolygonsMax){
            Polygon newPol = new Polygon();
            newPol.init();
            int pos = Tools.GetRand(0, Polygons_count);
            Polygons.add(pos, newPol);
            Polygons_count++;
            setDirty();
        }
    }

    private void ReplacePol(){ //Replace Polygon
        if (Polygons_count != 0){
            int idx = Tools.GetRand(0, Polygons_count - 1);
            Polygons.get(idx).init();
            setDirty();
        }
    }

    private void SwapPol(){ //Swap Polygon
        if (Polygons_count >= 2){
            int idx1 = Tools.GetRand(0, Polygons_count - 1);
            int idx2;
            do{
                idx2 = Tools.GetRand(0, Polygons_count - 1);
            }while(idx1 != idx2);
            Polygons.set(idx1, Polygons.set(idx2, Polygons.get(idx1)));
            setDirty();
        }
    }
}
