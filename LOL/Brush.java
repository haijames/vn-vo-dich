package LOL;

import static LOL.Settings.*;
import static LOL.Tools.*;

public class Brush {
    public int red;
    public int green;
    public int blue;
    public int alpha;

    public void init(){
        red = GetRand(0,255);
        green = GetRand(0,255);
        blue = GetRand(0,255);
        alpha = GetRand(0,255);
    }

    public Brush Clone(){
        Brush a = new Brush();
        a.red = red;
        a.blue = blue;
        a.green = green;
        a.alpha = alpha;
        return a;
    }

    public void Mutate (Drawing drawing){
        //Random Mutation
        if (WillMutate(RedMutationRate)){
            red = GetRand(0, 255);
            drawing.setDirty();
        }
        if (WillMutate(GreenMutationRate)){
            green = GetRand(0, 255);
            drawing.setDirty();
        }
        if (WillMutate(BlueMutationRate)){
            blue = GetRand(0, 255);
            drawing.setDirty();
        }
        if (WillMutate(AlphaMutationRate)){
            alpha = GetRand(1, 255);
            drawing.setDirty();
        }
        //Shift Mutation
        if (WillMutate(RedShiftMutationRate)){
            red += GetRand(-3, 3);
            red = Restrict(red,0,255);
        }
        if (WillMutate(BlueShiftMutationRate)){
            blue += GetRand(-3, 3);
            blue = Restrict(blue,0,255);
        }
        if (WillMutate(GreenShiftMutationRate)){
            green += GetRand(-3, 3);
            green = Restrict(green,0,255);
        }
        if (WillMutate(AlphaShiftMutationRate)){
            alpha += GetRand(-3, 3);
            alpha = Restrict(alpha,1,255);
        }
    }


}
