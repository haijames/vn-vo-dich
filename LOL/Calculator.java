package LOL;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;

import javax.tools.Tool;
import java.io.Serializable;

public class Calculator implements Serializable {
    public static double getdrawing(Image img, int[][] color) {
        double pixeroor = 0;
        PixelReader pxr = img.getPixelReader();
        for (int y = 0; y< Tools.MaxH; y++) //row
            for (int x = 0; x<Tools.MaxW; x++){ //column
                int c1 = pxr.getArgb(x,y);
                int c2 = color[x][y]; //sourceColor
                pixeroor += ColorFitness(c1,c2) / Tools.MaxW / Tools.MaxH;
            }
        return pixeroor;
    }

    private static double ColorFitness(int c1, int c2){
        double r = ((c1 >> 16) & 0xff) - ((c2 >> 16) & 0xff);
        double b = (c1 & 0xff) - (c2 & 0xff);
        double g = ((c1 >> 8) & 0xff) - ((c2 >> 8) & 0xff);
        return r*r + g*g + b*b;
    }


}
