package LOL;

import java.io.Serializable;
import static LOL.Settings.*;
import static LOL.Tools.*;

public class Point implements Serializable {
    public int x, y;

    Point(int X, int Y){
        x=X;
        y=Y;
    }

    Point(){ x = y = 0;}

    /**
     * Get the x axis coordinates
     * @return
     */
    int getX(){ return x; }

    /**
     * Get the y axis coordinates
     * @return
     */
    int getY(){ return y; }


    public void Init(){
        x = GetRand(0, MaxW - 1);
        y = GetRand(0, MaxH - 1);
    }

    public Point Clone(){
        return new Point(x, y);
    }

    public void Mutate(Drawing drawing){

        if (WillMutate(MovePointMaxMutationRate)){
            x = GetRand(0, MaxW);
            y = GetRand(0, MaxH);
            drawing.setDirty();
        }

        if (WillMutate(MovePointMidMutationRate)){
            x = Restrict(x + GetRand(-MovePointRangeMid, MovePointRangeMid),0, MaxW);
            y = Restrict(y + GetRand(-MovePointRangeMid, MovePointRangeMid),0, MaxH);
            drawing.setDirty();
        }

        if (WillMutate(MovePointMinMutationRate)){
            x = Restrict(x + GetRand(-MovePointRangeMin, MovePointRangeMin),0, MaxW);
            y = Restrict(y + GetRand(-MovePointRangeMin, MovePointRangeMin),0, MaxW);
            drawing.setDirty();
        }
    }
}
