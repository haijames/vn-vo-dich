package LOL;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;


public class Controller implements Serializable {

    @FXML
    public Image originImage;
    @FXML
    AnchorPane mainParent = new AnchorPane();
    @FXML
    public Window stage;
    @FXML
    private final Label fileLabel = new Label();
    @FXML
    StackPane sourceImagePane;
    @FXML
    StackPane resultImagePane = new StackPane();
    @FXML
    Button exitButton;
    @FXML
    Button backButton;
    @FXML
    Button startButton;
    @FXML
    Button stopButton;
    private File file = null;

    @FXML ImageView displayView = new ImageView();
    //
    private boolean setup, start = false, pause = false;
    Timeline timeline;
    int [][] sourcecolor;
    WritableImage display_img = null;

    Renderer renderer;

    public void initialize(Stage stage) {
        // TODO
    }


    @FXML
    private void start_Evolution(ActionEvent event){
        if (!start) {
            start = true;
            pause = false;

            startButton.setText("Pause");

            if (timeline != null) timeline.stop();
            if (setup) {
                setup = false;
                display_img = new WritableImage(Tools.MaxW, Tools.MaxH);
                Canvas canvas = new Canvas(Tools.MaxW, Tools.MaxH);
                GraphicsContext graphics_context = canvas.getGraphicsContext2D();

                renderer = new Renderer(originImage, canvas, graphics_context, display_img);
                renderer.initialize(sourcecolor);
                graphics_context.fillRect(0, 0, Tools.MaxW, Tools.MaxH);
                setupResultImage();
            } else {
                renderer.initialize(sourcecolor);
            }
            timeline.playFromStart();
        }
        else if (!pause){
            pause = true;
            timeline.pause();
            startButton.setText("Resume");
        }
        else {
            pause = false;
            timeline.play();
            startButton.setText("Pause");
        }

    }



    //convert the core image to a matrix for faster lookup
    private void Setupcorecolor(){
        double width = originImage.getWidth();
        double height = originImage.getHeight();
        Tools.MaxW= (int) width; Tools.MaxH = (int) height;
        sourcecolor = new int[Tools.MaxW][Tools.MaxH];
        PixelReader reader = originImage.getPixelReader();
        for (int x = 0; x < width; x++) for (int y = 0; y < height; y++){
            sourcecolor[x][y] = reader.getArgb(x, y);
        }
    }

    @FXML
    public void stopRender(ActionEvent event){
        startButton.setText("Start");
        if (renderer != null) renderer.reset(sourcecolor);
        timeline.stop();
        start = false;
        resultImagePane.getChildren().remove(displayView);
        mainParent.getChildren().remove(resultImagePane);
    }
    ////////////////////////////////////

    public void backAction(ActionEvent event) throws Exception {
        //Back to main stage
        FXMLLoader loader = new FXMLLoader(getClass().getResource("starter.fxml"));
        AnchorPane backMainParent = loader.load();
        Scene backToMainScene = new Scene(backMainParent);
        Stage scene = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene.setScene(backToMainScene);
        scene.show();
    }

    public void exitAction(){
        // get a handle to the stage
        Stage stage = (Stage) exitButton.getScene().getWindow();
        // do what you have to do
        stage.close();
    }

    void setupResultImage() {
        displayView.setImage(display_img);
        displayView.setFitHeight(450);
        displayView.setFitWidth(400);
        displayView.setPreserveRatio(true);
        displayView.setSmooth(true);
        displayView.setCache(true);
        resultImagePane.getChildren().add(displayView);
        resultImagePane.setLayoutX(400);
        resultImagePane.setPrefSize(400, 450);
        resultImagePane.setAlignment(Pos.CENTER);
        mainParent.getChildren().add(0, resultImagePane);
    }

    void setupOriginView() {
        ImageView imageView = new ImageView(originImage);
        imageView.setFitHeight(450);
        imageView.setFitWidth(400);
        imageView.setPreserveRatio(true);
        imageView.setSmooth(true);
        imageView.setCache(true);
        sourceImagePane.getChildren().clear();
        sourceImagePane.getChildren().add(imageView);
        sourceImagePane.setLayoutX(0.0);
        sourceImagePane.setPrefSize(400, 450);
        sourceImagePane.setAlignment(Pos.CENTER);

      //  mainParent.getChildren().add(0, sourceImagePane);
    }

    public void goAction(ActionEvent event) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("main.fxml"));
        mainParent = loader.load();
        Scene mainScene = new Scene(mainParent);
        //mainScene.getStylesheets().add(getClass().getResource("Style.css").toExternalForm());

        //Adding scene to stage
        Stage scene = (Stage) ((Node) event.getSource()).getScene().getWindow();
        //initialize(scene);
        //this.stage = stage;

        //Display contents of the stage
        scene.setScene(mainScene);
        scene.show();
    }

    public void openImageAction(ActionEvent event) throws Exception {
        FileChooser fileChooser = new FileChooser();
        //Filter for images
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Picture Files", "*.*"),
                new FileChooser.ExtensionFilter("PNG (*.png)", "*.png"),
                new FileChooser.ExtensionFilter("JPEG (*.jpg;*.jpeg;*.jpe;)", "*.jpg;*.jpeg;*.jpe")
        );

        fileChooser.setTitle("Open Picture Files");
        file = fileChooser.showOpenDialog(stage);

        if (file != null) {
            fileLabel.setText(file.getPath());
            originImage = new Image(file.toURI().toString());
            setupOriginView();
            setup = true;
            Setupcorecolor();

            timeline = new Timeline();
            timeline.setCycleCount(1);
            timeline.setOnFinished(actionEvent -> timeline.play());

            KeyFrame keyframe = new KeyFrame(Duration.ONE, actionEvent ->{
                ///bla bla bla do something i dont know yet
                renderer.Render(sourcecolor);
                renderer.render_best_generation();

            });
            timeline.getKeyFrames().add(keyframe);
        }
    }
}
