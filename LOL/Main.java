package LOL;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {


    //private Controller my_controller;
    @FXML

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("starter.fxml"));
        Parent root = loader.load();

        //Set title
        primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("icon.png")));
        primaryStage.setTitle("Evolving Semi-Transparent Polygons into Work of Art");
        //my_controller = loader.getController();
        //my_controller.initialize(primaryStage);
        Scene scene = new Scene(root);

        //Adding scene to stage
        primaryStage.setScene(scene);

        //Display contents of the stage
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
