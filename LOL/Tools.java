package LOL;

import java.util.Random;

public class Tools {

    private static Random rand = new Random();

    public static int MaxH = 600;

    public static int MaxW = 900;

    public static int GetRand(int min, int max){
        return (min == max) ? min : rand.nextInt(max-min) + min;
    }

    public static boolean WillMutate(int rate){
        //return (rate * 100 >= Math.abs(rand.nextInt()));
        return (GetRand(0,10*rate) == 69);
    }

    public static int Restrict(int value, int min, int max){
        return value < min ? min : value > max ? max : value;
    }
}
